<?php

namespace App\Http\Controllers;

use App\DataTables\StudentsDataTable;
use Illuminate\Http\Request;

class StudentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:admin-students');
    }

    public function index(StudentsDataTable $dataTable)
    {
        return $dataTable->render("pages.admin.students.index");
    }
}
