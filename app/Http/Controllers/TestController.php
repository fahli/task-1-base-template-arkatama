<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:test');
    }

    public function index()
    {
        return "OK";
    }
}
