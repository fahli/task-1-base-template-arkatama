<?php

namespace App\Models;

use App\Traits\HasUuid;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class Student extends Model
{
    use HasFactory, HasUuid;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name',
        'nim',
        'email',
        'address',
        'phone_number',
        'gender',
        'birth_place',
        'birth_date',
        'photo',
    ];

    /**
     * Add event listener to delete photo when student is deleted
     *
     * @return void
     */
    protected static function booted() {
        static::deleting(function ($model) {
            if ($model->attributes['photo']) {
                Storage::disk('public')->delete($model->attributes['photo']);
            }
        });
    }

    /**
     * Accessor for photo attribute to get photo url
     *
     * @return string
     */
    public function getPhotoAttribute() : string
    {
        return $this->attributes['photo'] ? asset('storage/' . $this->attributes['photo']) : asset('assets/media/avatars/blank.png');
    }

    /**
     * Mutator for photo attribute to save photo to storage
     *
     * @param \Illuminate\Http\UploadedFile|null $value Student photo
     * @return void
     */
    public function setPhotoAttribute($value) : void
    {
        $this->savePhoto($value); // save photo to storage
    }

    /**
     * Accessor for birth date attribute to get formatted birth date
     *
     * @return string
     */
    // public function getBirthDateAttribute() : string
    // {
    //     return $this->attributes['birth_date'] ? Carbon::parse($this->attributes['birth_date'])->format('d F Y') : '';
    // }

    /**
     * Display birth place and birth date
     *
     * Example: Jakarta, 17 Aug 1999
     *
     * @return string
     */
    public function getDisplayBirthDateAttribute() : string
    {
        $birth_date = $this->birth_date;
        $birth_place = $this->birth_place;
        if ($birth_date) {
            $birth_date = Carbon::parse($birth_date)->format('d M Y');
        }

        if ($birth_place) {
            return $birth_place . ', ' . $birth_date;
        } else {
            return $birth_date ?? '-';
        }
    }

    /**
     * Save student photo to storage and database
     *
     * @param \Illuminate\Http\UploadedFile|null $photo Student photo
     * @return void
     */
    private function savePhoto(UploadedFile $photo = null) : void
    {
        if (!$photo) {
            return;
        }
        // Delete old photo
        if (isset($this->attributes['photo'])) {
            Storage::disk('public')->delete($this->attributes['photo']);
        }

        // Store new photo
        $path = Storage::disk('public')->put('students', $photo);
        $this->attributes['photo'] = $path;
        $this->save();
    }
}
