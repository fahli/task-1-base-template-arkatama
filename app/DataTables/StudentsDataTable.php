<?php

namespace App\DataTables;

use App\Helpers\ColumnHelper;
use App\Helpers\DataTableHelper;
use App\Models\Student;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Illuminate\Support\Carbon;
use Livewire\Livewire;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class StudentsDataTable extends DataTable
{
    /**
     * Build the DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->addColumn('checkbox', function ($row) {
                return DataTableHelper::checkbox($row, $row->full_name);
            })
            ->addColumn('action', 'students.action')
            ->addIndexColumn()
            ->editColumn('full_name', function (Student $val) {
                // with avatar
                return <<<HTML
                    <div class="d-flex align-items-center">
                        <div class="symbol symbol-35px me-5">
                            <img src="{$val->photo}" class="symbol-label" alt="photo">
                        </div>
                        <div class="d-flex justify-content-start flex-column">
                            <a href="#" class="text-dark fw-bolder text-hover-primary mb-1 fs-6">{$val->full_name}</a>
                            <span class="text-muted
                            fw-bold text-muted d-block fs-7">{$val->nim}</span>
                        </div>
                    </div>
                HTML;
            })
            ->addColumn('action', function (Student $val) {
                return Livewire::mount('pages.admin.students.student-table-action', ['student' => $val]);
            })
            ->editColumn('gender', function(Student $val) {
                return $val->gender == "L" ? "Laki-laki" : "Perempuan";
            })
            ->rawColumns(['action', 'full_name', 'checkbox'])
            ->setRowId('id');
    }

    /**
     * Get the query source of dataTable.
     */
    public function query(Student $model): QueryBuilder
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use the html builder.
     */
    public function html(): HtmlBuilder
    {
        return DataTableHelper::builder($this, 'students-table')
                ->orderBy(3)
                ->buttons([]);
    }

    /**
     * Get the dataTable columns definition.
     */
    public function getColumns(): array
    {
        return [
            DataTableHelper::addCheckbox(),
            Column::computed("DT_RowIndex")
                ->title("No.")
                ->width(20),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center'),
            ColumnHelper::make('full_name')->title('Nama Lengkap')->setFilterAsText()->set(),
            Column::make('nim')->hidden(),
            ColumnHelper::make('gender')->title('Jenis Kelamin')->setFilterAsSelect(['L' => 'Laki-laki', 'P' => 'Perempuan'])->set(),
            ColumnHelper::make('birth_place')->title('Tempat Lahir')->setFilterAsText()->set(),
            ColumnHelper::make('birth_date')->title('Tanggal Lahir')->setFilterAsDate()->set(),
            ColumnHelper::make('phone_number')->title('Nomor Telepon')->setFilterAsText()->set(),
            ColumnHelper::make('email')->title('Email')->setFilterAsText()->set(),
        ];
    }

    /**
     * Get the filename for export.
     */
    protected function filename(): string
    {
        return 'Students_' . date('YmdHis');
    }
}
