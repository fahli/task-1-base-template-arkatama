<?php

namespace App\Livewire\Forms;

use Livewire\Attributes\Validate;
use Livewire\Features\SupportFileUploads\WithFileUploads;
use Livewire\Form;

class StudentForm extends Form
{
    use WithFileUploads;

    #[Validate]
    public $id;
    public $full_name;
    public $nim;
    public $email;
    public $gender;
    public $birth_place;
    public $birth_date;
    public $address;
    public $phone_number;
    public $photo;

    public $photo_url = '/assets/media/avatars/blank.png'; // default photo url

    /**
     * Rules for validation
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'full_name' => 'required|min:3|max:150',
            'nim' => 'required|size:12|unique:students,nim,' . $this->id,
            'email' => 'required|email|max:255',
            'gender' => 'required|in:L,P',
            'birth_place' => 'required|min:3|max:150',
            'birth_date' => 'required|date',
            'address' => 'required|min:3|max:255',
            'phone_number' => 'required|numeric|digits_between:10,15',
            'photo' => 'nullable|mimes:jpg,jpeg,png|max:2048',
        ];
    }
}
