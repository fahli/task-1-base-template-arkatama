<?php

namespace App\Livewire\Components;

use App\Models\Student;
use Livewire\Attributes\On;
use Livewire\Component;

class DatatableCheckbox extends Component
{
    public $model;
    public $table;
    public function render()
    {
        return view('livewire.components.datatable-checkbox');
    }

    /**
     * Delete selected students
     *
     * @param null|array $ids Student IDs
     * @return void
     */
    #[On('delete')]
    public function delete($ids) : void {
        foreach ($ids as $id) {
            Student::destroy($id);
        }

        $this->dispatch('deleted', count($ids));
    }
}
