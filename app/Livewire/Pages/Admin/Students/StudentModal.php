<?php

namespace App\Livewire\Pages\Admin\Students;

use App\Livewire\Forms\StudentForm;
use App\Models\Student;
use Livewire\Attributes\On;
use Livewire\Component;
use Livewire\Features\SupportFileUploads\WithFileUploads;

class StudentModal extends Component
{
    use WithFileUploads;
    public StudentForm $form;

    /**
     * Render the component
     *
     * @return void
     */
    public function render()
    {
        return view('livewire.pages.admin.students.student-modal');
    }

    /**
     * Save student data
     *
     * This method will validate the form and save the student data
     * @return void
     */
    public function save() : void
    {
        $this->validate();
        $student = Student::updateOrCreate(['id' => $this->form->id], $this->form->all());

        if($student){
            $this->dispatch('student-saved');
            $this->form->reset();
        }
    }

    /**
     * Edit student by ID
     *
     * @param null|string $id Student ID
     * @return void
     */

    #[On('edit')]
    public function edit($id) : void
    {
        $this->form->reset(); // reset first

        // find student by id, exclude photo attribute
        $student = Student::findOrFail($id);
        $this->form->photo_url = $student->photo;
        $student->makeHidden('photo');
        
        // re-populate form with student data
        $this->form->fill($student->toArray());
        $this->dispatch('student-edit');
    }

    /**
     * Delete student by ID
     *
     * @param null|string $id Student ID
     * @return void
     */
    #[On('delete')]
    public function delete($id) : void
    {
        $student = Student::destroy($id);
        if($student){
            $this->dispatch('student-deleted');
        }
    }
}
