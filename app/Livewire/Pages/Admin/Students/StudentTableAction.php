<?php

namespace App\Livewire\Pages\Admin\Students;

use Livewire\Component;

class StudentTableAction extends Component
{
    public $student;
    public function mount($student){
        $this->student = $student;
    }
    public function render()
    {
        return view('livewire.pages.admin.students.student-table-action');
    }
}
