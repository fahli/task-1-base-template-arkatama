<?php

namespace App\Helpers;

use Yajra\DataTables\Html\Column;

class ColumnHelper {

    private static Column $column;
    private static string $filterType;
    private static array $items;

    public static function make(string $column) {
        self::$column = Column::make($column);
        return new self;
    }

    public function title(string $title) {
        self::$column->title($title);
        return $this;
    }

    public function setFilterAsText() {
        self::$filterType = 'text';
        return $this;
    }

    public function setFilterAsSelect(array $items = []) {
        self::$filterType = 'select';
        self::$items = $items;
        return $this;
    }

    public function setFilterAsDate() {
        self::$filterType = 'date';
        return $this;
    }

    public function set() {
        self::$column->attributes(['data-filter-type' => self::$filterType]);
        if (self::$filterType == 'select') {
            self::$column->attributes(['data-filter-items' => json_encode(self::$items)]);
        }
        return self::$column;
    }

}