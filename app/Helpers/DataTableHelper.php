<?php

namespace App\Helpers;

use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Column;

class DataTableHelper {
    public static function builder(DataTable $dataTable, string $tableId) : HtmlBuilder {
        return $dataTable->builder()
            ->setTableId('students-table')
            ->columns($dataTable->getColumns())
            ->minifiedAjax(script: "
                data._token = '" . csrf_token() . "';
                data._p = 'POST';
            ")
            ->dom('rt' . "<'row'<'col-sm-12 col-md-5'l><'col-sm-12 col-md-7'p>>",)
            ->addTableClass('table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer text-gray-600 fw-semibold')
            ->setTableHeadClass('text-start text-muted fw-bold fs-7 text-uppercase gs-0')
            ->orderCellsTop(true)
            ->fixedHeader(true)
            ->drawCallbackWithLivewire(file_get_contents(resource_path('js/custom/table/_init.js')))
            ->parameters([
                'initComplete' => file_get_contents(resource_path('js/custom/table/callback.js')),
            ])
            ->select(false);
    }


    /**
     * Add checkbox column to DataTable
     *
     * @return Column Checkbox column
     */
    public static function addCheckbox() : Column {
        $col = Column::computed('checkbox')->title(<<<HTML
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="checkbox-all" name="checkbox-all">
                        <label class="form-check-label" for="checkbox-all"></label>
                    </div>
                    HTML)
            ->addClass('table-column-pe-0')
            ->exportable(false)
            ->printable(false);

        $col->attributes = ['scope' => 'col'];

        return $col;
    }

    /**
     * Add checkbox to each row in DataTable
     *
     * @param object $row Row data
     * @param string|null $name Name to saved in checkbox attribute data-name (default: $row->name)
     * @return string Checkbox HTML element
     */
    public static function checkbox(object $row, string $name = null) : string {
        $name = $name ?: $row->name;
        return <<<HTML
            <div class="form-check">
                <input type="checkbox" class="form-check-input mass-delete" id="checkbox-{$row->id}" name="checkbox[]" value="{$row->id}" data-name="{$name}">
                <label class="form-check-label" for="checkbox-{$row->id}"></label>
            </div>
        HTML;
    }
}