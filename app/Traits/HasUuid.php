<?php
namespace App\Traits;

use Illuminate\Support\Str;

trait HasUuid
{
    /**
     * Get the primary key name
     * 
     * @return string
     */
    public function getKeyName()
    {
        return 'id';
    }

    /**
     * Get the data type for the primary key
     * 
     * @return string
     */
    public function getKeyType()
    {
        return 'string';
    }

    /**
     * Get the value indicating whether the IDs are incrementing
     * 
     * @return false
     */
    public function getIncrementing()
    {
        return false;
    }

    /**
     * Boot the trait
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            try {
                if ($model->id === null) {
                    $model->id = Str::uuid()->toString();
                }
            } catch (\Exception $e) {
                abort(500, $e->getMessage());
            }
        });
    }
}
