$('[data-action-delete]').each(function (element) {
    $(this).on('click', function () {
        const livewireInstance = getLivewireInstance($(this));
        const actionParams = $(this).data('action-params');
        const actionName = $(this).data('action');
        const actionReceiver = $(this).data('action-receiver');
    
        Swal.fire({
            text: 'Are you sure you want to remove?',
            icon: 'warning',
            buttonsStyling: false,
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            customClass: {
                confirmButton: 'btn btn-danger',
                cancelButton: 'btn btn-secondary',
            }
        }).then((result) => {
            if (result.isConfirmed) {
                livewireInstance.dispatchTo(actionReceiver,actionName,{
                    ...actionParams,
                });
            }
        });
    });
});

$('[data-action-search]').on('change', function(){
    const tableId = $(this).data('table-id');
    const dataTable = window.LaravelDataTables[`${tableId}`];
    dataTable.search(this.value).draw();
});

const checkboxAll = $('#checkbox-all');
const checkboxes = $('td:first-child input[type="checkbox"]');
const datatableCounterInfo = $('#datatableCounterInfo');
const datatableCounter = $('#datatableCounter');
const datatable = this.api();

checkboxAll.prop('checked', false);
datatableCounterInfo.addClass('d-none');

checkboxAll.on('change', function() {
    const checked = $(this).is(':checked');
    checkboxes.prop('checked', checked);
    checkboxes.trigger('change');
});

checkboxes.on('change', function() {
    const checked = $(this).is(':checked');
    const totalCheckboxes = checkboxes.length;
    const totalChecked = $('td:first-child input[type="checkbox"]:checked').length;

    if (checked) {
        if (totalChecked === totalCheckboxes) {
            checkboxAll.prop('checked', true);
        }
    } else {
        checkboxAll.prop('checked', false);
    }

    datatableCounter.text(totalChecked);
    if (totalChecked > 0) {
        datatableCounterInfo.removeClass('d-none');
    } else {
        datatableCounterInfo.addClass('d-none');
    }
});