function() {

    $('thead tr')
        .clone(true)
        .addClass('filters')
        .appendTo('thead');

    $('.filters th').not(':eq(0)').each(function () {
        let attributes = $.map(this.attributes, function(item) {
            if (!item.name.includes('data-filter-')) {
                return item.name;
            } else {
                return null;
            }
        });
        const el = $(this);
        el.off('click');
        $.each(attributes, function(i, item) {
            el.removeAttr(item);
        });
    });

    function sanitize(string) {
        return string.replace(/[^a-zA-Z ]/g, '');
    }


    const api = this.api();
    api
    .columns()
    .eq(0)
    .each(function (colIdx) {
        // Set the header cell to contain the input element
        var cell = $('.filters th').eq(
            $(api.column(colIdx).header()).index()
        );
        var title = $(cell).text();
        $(cell).html('');

        const attributes = $.map(cell[0].attributes, function(item) {
            return item.name;
        });

        if (attributes.includes('data-filter-items')) {
            const select = $('<select class="filter-select form-select form-select-solid" data-control="select2"><option value="">' + title + '</option></select>')
                .appendTo($(cell))
                .on('change', function () {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );
                    api
                        .column(colIdx)
                        .search(val ? '^' + val + '$' : '', true, false)
                        .draw();
                });

            // parse json from data-filter-items attribute
            let items = [];
            try {
                items = JSON.parse(cell.attr('data-filter-items'));

            } catch (error) {
            }

            // count items
            let count = Object.keys(items).length;
            if (count > 0) {
                for (const key in items) {
                    if (items.hasOwnProperty(key)) {
                        const value = items[key];
                        select.append('<option value="' + key + '">' + value + '</option>');
                    }
                }
            } else {
                api
                    .column(colIdx)
                    .data()
                    .unique()
                    .sort()
                    .each(function (d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>');
                    });
            }
        } else if (attributes.includes('data-filter-type')) {
            const type = cell.attr('data-filter-type');
            switch (type) {
                case 'text':
                    $(cell).html('<input class="form-control form-control-solid" type="text" placeholder="' + title + '" />');
                    break;
                case 'date':
                    $(cell).html('<input class="form-control-solid form-control filter-custom-datepicker" type="text" placeholder="' + title + '" />');
                    break;
                default:
                    $(cell).html('');
                    break;
            }
        }

        // On every keypress in this input
        $(
            'input',
            $('.filters th').eq($(api.column(colIdx).header()).index())
        )
            .off('keyup change')
            .on('change', function (e) {
                // Get the search value
                $(this).attr('title', $(this).val());
                var regexr = '({search})'; //$(this).parents('th').find('select').val();
                let searchString = sanitize(this.value);
                api
                    .column(colIdx)
                    .search(
                        searchString != ''
                            ? regexr.replace('{search}', '(((' + searchString + ')))')
                            : '',
                            searchString != '',
                            searchString == ''
                    )
                    .draw();
            })
            .on('keydown', function (e) {
                e.stopPropagation();
                if (e.keyCode == 65 && e.ctrlKey) {
                    e.target.select()
                }
            })
            .on('keyup', function (e) {
                e.stopPropagation();

                var cursorPosition = this.selectionStart;
                $(this).trigger('change');
                $(this)
                    .focus()[0]
                    .setSelectionRange(cursorPosition, cursorPosition);
            });
    });

    $('.filter-custom-datepicker').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        minYear: 1901,
        maxYear: parseInt(moment().format('YYYY'), 10),
        locale: {
            format: 'YYYY-MM-DD',
            cancelLabel: 'Clear',
        },
        autoUpdateInput: false,
    });

    $('.filter-custom-datepicker').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD'));
        $(this).trigger('change');
    });
  
    $('.filter-custom-datepicker').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        $(this).trigger('change');
    });

    $('[data-action-delete]').each(function (element) {
        $(this).on('click', function () {
            const livewireInstance = getLivewireInstance($(this));
            const actionParams = $(this).data('action-params');
            const actionName = $(this).data('action');
            const actionReceiver = $(this).data('action-receiver');
        
            Swal.fire({
                text: 'Are you sure you want to remove?',
                icon: 'warning',
                buttonsStyling: false,
                showCancelButton: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
                customClass: {
                    confirmButton: 'btn btn-danger',
                    cancelButton: 'btn btn-secondary',
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    livewireInstance.dispatchTo(actionReceiver,actionName,{
                        ...actionParams,
                    });
                }
            });
        });
    });
}