<x-layouts.app>
    <x-slot:title>Mahasiswa</x-slot:title>
    <livewire:pages.admin.students.student-modal />

    <div class="card">
        <div class="card-header border-0 pt-6">
            <div class="card-title">
                {{-- <div class="me-2">
                    <x-atoms.select id="filter" class="form-select-solid">
                        <option value="2" selected>Nama</option>
                        <option value="3">NIM</option>
                        <option value="4">Gender</option>
                        <option value="5">Tempat Lahir</option>
                        <option value="6">Tanggal Lahir</option>
                        <option value="7">No. Telp</option>
                        <option value="8">Email</option>
                    </x-atoms.select>
                </div> --}}
                <div class="d-flex align-items-center position-relative my-1 me-2">
                    <span class="ki-outline ki-magnifier fs-3 position-absolute ms-5"></span>
                    <input type="text" data-action-search data-table-id="students-table"
                        class="form-control form-control-solid w-250px ps-13" placeholder="Search students"
                        id="mySearchInput" />
                </div>
            </div>

            <div class="card-toolbar">
                <!--begin::Toolbar-->
                <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                    {{-- <livewire:components.datatable-checkbox :model="\App\Models\Student::class" :table="students-table" /> --}}
                    @livewire('components.datatable-checkbox', [
                        'model' => \App\Models\Student::class,
                        'table' => 'students-table'
                    ])
                    <!--begin::Add user-->
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                        data-bs-target="#student-modal">
                        <i class="ki-duotone ki-plus fs-2"></i>
                        <span>Tambah Mahasiswa</span>
                    </button>
                </div>
            </div>
        </div>

        <div class="card-body py-4">
            <div class="table-responsive">
                {{ $dataTable->table() }}
            </div>
        </div>
    </div>
    @push('scripts')
        {{ $dataTable->scripts() }}
        {{-- <script>
            $(document).ready(function () {
                const filter = document.getElementById('filter');
                const search = document.getElementById('mySearchInput');
                const table = window.LaravelDataTables['students-table'];
                filter.addEventListener('change', function() {
                    search.placeholder = `Cari berdasarkan ${filter.options[filter.selectedIndex].text}`;
                    doSearch();
                });
                search.addEventListener('keyup', function() {
                    doSearch();
                });

                function doSearch() {
                    const value = search.value.toLowerCase();
                    if (value === '') {
                        table.search('').draw();
                        return;
                    }
                    const filterValue = filter.value;
                    table.columns(filterValue).search(value).draw();
                
                }
                window.addEventListener('load', function() {
                    // trigger keyup event
                    const event = new Event('change');
                    filter.dispatchEvent(event);
                });
            });
        </script> --}}
    @endpush
</x-layouts.app>
