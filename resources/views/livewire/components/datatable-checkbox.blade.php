<div id="datatableCounterInfo" class="d-none">
    <div class="d-flex align-items-center me-3">
        <span class="fs-5 me-5">
            <span id="datatableCounter">0</span>
            Dipilih
        </span>

        <button class="btn btn-danger" id="btn_mass_delete" data-livewire-instance="@this" data-action="delete"
            data-action-receiver="components.datatable-checkbox">
            <i class="ki-solid ki-trash fs-2"></i> Hapus
        </button>

    </div>
</div>

@push('scripts')
<script>
    document.addEventListener('livewire:initialized', () => {
        function refreshTable() {
            window.LaravelDataTables['{{ $table }}'].ajax.reload();
        };
        @this.on('deleted', (count) => {
            refreshTable();
            Swal.fire({
                icon: 'success',
                title: 'Berhasil',
                text: count + ' data berhasil dihapus!',
            });
        });

        
        $("#btn_mass_delete").on('click', function() {
            const checked = $('td:first-child input[type="checkbox"]:checked');
            const ids = [];
            checked.each(function() {
                ids.push($(this).val());
            });
            const livewireInstance = getLivewireInstance($(this));
            const actionReceiver = $(this).data('action-receiver');
            const actionName = $(this).data('action');

            // encode the ids to json

            const params = {
                ids: ids
            };

            const counter = ids.length;
            Swal.fire({
                text: 'Are you sure you want to remove ' + counter + ' items?',
                icon: 'warning',
                buttonsStyling: false,
                showCancelButton: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
                customClass: {
                    confirmButton: 'btn btn-danger',
                    cancelButton: 'btn btn-secondary',
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    const data = livewireInstance.dispatchTo(actionReceiver,actionName,{
                        ...params
                    });
                }
            });
        });
    })
</script>
@endpush