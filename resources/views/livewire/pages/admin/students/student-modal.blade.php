<div>
    <x-mollecules.modal id="student-modal" action="save" wire:ignore.self>
        @if (isset($form->id))
            <x-slot:title>Edit Mahasiswa</x-slot:title>
        @else
            <x-slot:title>Tambah Mahasiswa</x-slot:title>
        @endif
        <div class="mb-6 d-flex">
            <div class="me-5">
                @if($form->photo)
                    <div class="symbol symbol-100px">
                        <img src="{{ $form->photo->temporaryUrl() }}" class="symbol-label" alt="photo">
                    </div>
                @else
                    <div class="symbol symbol-100px">
                        <img src="{{ $form->photo_url }}" class="symbol-label" alt="photo">
                    </div>
                @endif
            </div>
            <div>
                <x-atoms.form-label required>Foto Mahasiswa</x-atoms.form-label>
                <x-atoms.file name="photo" wire:model='form.photo' />
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 mb-6">
                <x-atoms.form-label required>Nama Lengkap</x-atoms.form-label>
                <x-atoms.input name="full_name" wire:model='form.full_name' />
            </div>
            <div class="col-md-4 mb-6">
                <x-atoms.form-label required>NIM</x-atoms.form-label>
                <x-atoms.input name="nim" wire:model='form.nim' />
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 mb-6">
                <x-atoms.form-label required>Jenis Kelamin</x-atoms.form-label>
                <select wire:model="form.gender" class="form-select " data-control="select2" data-hide-search="true"
                    data-placeholder="Pilih jenis kelamin" name="gender">
                    <option value="">Pilih jenis kelamin</option>
                    <option value="L">Laki-laki</option>
                    <option value="P">Perempuan</option>
                </select>
            </div>
            <div class="col-md-4 mb-6">
                <x-atoms.form-label required>Tempat Lahir</x-atoms.form-label>
                <x-atoms.input name="birth_place" wire:model='form.birth_place' />
            </div>
            <div class="col-md-4 mb-6">
                <x-atoms.form-label required>Tanggal Lahir</x-atoms.form-label>
                <x-atoms.datepicker name="birth_date" wire:model='form.birth_date' />
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 mb-6">
                <x-atoms.form-label required>Nomor Telepon</x-atoms.form-label>
                <x-atoms.input name="phone_number" wire:model='form.phone_number' />
            </div>
            <div class="col-md-6 mb-6">
                <x-atoms.form-label required>Email</x-atoms.form-label>
                <x-atoms.input name="email" wire:model='form.email' />
            </div>
        </div>
        <div class="mb-6">
            <x-atoms.form-label required>Alamat</x-atoms.form-label>
            <x-atoms.textarea name="address" wire:model='form.address' />
        </div>
        <x-slot:footer>
            <button wire:loading.attr="disabled" class="btn-primary btn" type="submit">Submit</button>
        </x-slot:footer>
    </x-mollecules.modal>
</div>

@push('scripts')
    <script>
        document.addEventListener('livewire:initialized', () => {
            function refreshTable() {
                window.LaravelDataTables['students-table'].ajax.reload();
            };
            @this.on('student-saved', () => {
                $('#student-modal').modal('hide');
                refreshTable();
                Swal.fire({
                    icon: 'success',
                    title: 'Berhasil',
                    text: 'Data mahasiswa berhasil disimpan!',
                });
            })
            @this.on('student-edit', () => {
                $('#student-modal').modal('show');
                refreshTable();
            })
            @this.on('student-deleted', () => {
                refreshTable();
                Swal.fire({
                    icon: 'success',
                    title: 'Berhasil',
                    text: 'Data mahasiswa berhasil dihapus!',
                });
            })
        })
    </script>
@endpush
