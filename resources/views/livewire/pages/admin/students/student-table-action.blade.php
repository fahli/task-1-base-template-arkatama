<div class="d-flex justify-content-center align-items-center gap-2">
    @can('admin-students-update')
        <button wire:click="$dispatchTo('pages.admin.students.student-modal','edit', { id: '{{ $student->id }}' })"
            class="btn btn-light btn-active-light-primary p-3 btn-center btn-sm">
            <i class="ki-outline ki-pencil fs-2"></i>
        </button>
    @endcan
    @can('admin-students-delete')
        <button data-action-params='{{ json_encode(['id' => $student->id]) }}' data-livewire-instance="@this" data-action="delete"
            data-action-receiver="pages.admin.students.student-modal" data-action-delete
            class="btn btn-light btn-active-light-primary p-3 btn-center btn-sm">
            <i class="ki-outline ki-trash fs-2"></i>
        </button>
    @endcan
</div>
