<input
  @error($attributes->get('wire:model'))
    {{ $attributes->merge(["class" => "form-control is-invalid custom-datepicker"]) }}
  @else
    {{ $attributes->merge(["class" => "form-control custom-datepicker"]) }}
  @enderror
  type="text"
/>
@error($attributes->get('wire:model'))
  <small class="text-danger"> {{ $message }} </small>
@enderror


@push('scripts')
  <script>
    $('.custom-datepicker').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        minYear: 1901,
        maxYear: parseInt(moment().format('YYYY'), 10),
        locale: {
            format: 'YYYY-MM-DD'
        },
        autoUpdateInput: true,
        autoApply: true,
    }, function(start, end, label) {
      @this.set('{{ $attributes->get('wire:model') }}', start.format('YYYY-MM-DD'));
    });
  </script>
@endpush
