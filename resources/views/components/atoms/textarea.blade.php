@props([
  "autosize" => true
])

@error($attributes->get('wire:model'))
  <textarea @if($autosize) data-kt-autosize="true" @endif {{ $attributes->merge(["class" => "form-control is-invalid"]) }}>
    {{ $slot }}
  </textarea>
@else
<textarea @if($autosize) data-kt-autosize="true" @endif {{ $attributes->merge(["class" => "form-control"]) }}>
  {{ $slot }}
</textarea>
@enderror

@error($attributes->get('wire:model'))
  <small class="text-danger"> {{ $message }} </small>
@enderror
