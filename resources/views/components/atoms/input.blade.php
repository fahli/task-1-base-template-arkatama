<input
  @error($attributes->get('wire:model'))
    {{ $attributes->merge(["class" => "form-control is-invalid"]) }}
  @else
    {{ $attributes->merge(["class" => "form-control"]) }}
  @enderror
  type="text"
/>

@error($attributes->get('wire:model'))
  <small class="text-danger"> {{ $message }} </small>
@enderror
