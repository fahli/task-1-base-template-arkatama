<input
  @error($attributes->get('wire:model'))
    {{ $attributes->merge(["class" => "form-control is-invalid"]) }}
  @else
    {{ $attributes->merge(["class" => "form-control"]) }}
  @enderror
  type="file" />

<div class="mt-2" wire:loading wire:target="{{ $attributes->get('wire:model') }}">
  <div class="spinner-border spinner-border-sm text-primary" role="status">
    <span class="visually-hidden">Loading...</span>
  </div>
  <span>
    Menunggah foto...
  </span>
</div>

@error($attributes->get('wire:model'))
  <small class="text-danger"> {{ $message }} </small>
@enderror
